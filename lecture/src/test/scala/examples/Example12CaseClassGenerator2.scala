package examples

import org.scalacheck.Gen
import org.scalacheck.Gen.{choose, oneOf}

sealed trait Tree
final case class Node(left: Tree, right: Tree) extends Tree
final case class Leaf(value: Int) extends Tree

object Example12CaseClassGenerator2 extends App {

  def genTree(max: Int): Gen[Tree] = if (max > 0) oneOf(genLeaf, genNode(max)) else genLeaf

  val genLeaf = choose(0, 100).map(Leaf)
  def genNode(max: Int): Gen[Node] = for {
    left  <- genTree(max - 1)
    right <- genTree(max - 1)
  } yield Node(left, right)

  println(genTree(10).sample)
  println(genTree(10).sample)

  val listGen = Gen.listOf(Gen.alphaNumStr)



}
