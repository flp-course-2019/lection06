package examples

import org.scalacheck._
import org.scalacheck.Properties

object Example19Shrinking2 extends Properties("shrinking") {

  val shrinker: Shrink[List[Int]] = implicitly[Shrink[List[Int]]]

  val s1: Stream[List[Int]] = shrinker.shrink(List(1, 2, 3, 4))
  val s2: Stream[List[Int]] = shrinker.shrink(List(1, 2))
  val s3: Stream[List[Int]] = shrinker.shrink(List(1))

  println(s1.force)
  println(s2.force)
  println(s3.force)


}
