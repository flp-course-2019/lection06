package examples

import org.scalacheck.{Gen, Properties}

object Example15GeneratingContainers extends Properties("containers") {

  val genIntList: Gen[List[Int]]           = Gen.containerOf[List,Int](Gen.oneOf(1, 3, 5))
  val genStringStream: Gen[Stream[String]] = Gen.containerOf[Stream,String](Gen.alphaStr)
  val genBoolArray: Gen[Array[Boolean]]    = Gen.containerOf[Array,Boolean](true)


  val zeroOrMoreDigits: Gen[Seq[Int]] = Gen.someOf(1 to 9)
  val oneOrMoreDigits: Gen[Seq[Int]]  = Gen.atLeastOne(1 to 9)


  val fiveDice: Gen[Seq[Int]] = Gen.pick(5, 1 to 6)
  val threeLetters: Gen[Seq[Char]] = Gen.pick(3, 'A' to 'Z')

  val gens = List(genIntList, genStringStream, genBoolArray, zeroOrMoreDigits, oneOrMoreDigits, fiveDice, threeLetters)
  gens.foreach(g => println(g.sample))

}
