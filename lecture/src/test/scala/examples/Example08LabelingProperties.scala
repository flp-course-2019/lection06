package examples

import org.scalacheck.Prop.{BooleanOperators, forAll, all}
import org.scalacheck.Properties

object Example08LabelingProperties extends Properties("labeling") {

  def myMagicFunction(n: Int, m: Int): Int = n + m

  property("myMagicFunction1") = forAll { (n: Int, m: Int) =>
    val res = myMagicFunction(n, m)

    (res >= m)    :| "result > #1" &&
    (res >= n)    :| "result > #2" &&
    (res < m + n) :| "result not sum"
  }

  property("myMagicFunction2") = forAll { (n: Int, m: Int) =>
    val res = myMagicFunction(n, m)

    ("result > #1"    |: res >= m) &&
    ("result > #2"    |: res >= n) &&
    ("result not sum" |: res < m + n)
  }

  property("multiplication") = forAll { (n: Int, m: Int) =>
    val res = n * m

    ("evidence = " + res) |: all(
      "div1" |: m != 0 ==> (res / m == n),
      "div2" |: n != 0 ==> (res / n == m),
      "lt1"  |: res > m,
      "lt2"  |: res > n
    )
  }


}
