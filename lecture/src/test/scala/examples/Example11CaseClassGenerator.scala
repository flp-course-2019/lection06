package examples

import org.scalacheck.Prop.forAll
import org.scalacheck.{Arbitrary, Gen}

object Example11CaseClassGenerator extends App {

  sealed trait Gender
  final case object Male   extends Gender
  final case object Female extends Gender

  final case class User(name: String, age: Int, gender: Gender)
  val userGenerator = for {
    name <- Arbitrary.arbitrary[String]
    age  <- Gen.choose(1, 100)
    gndr <- Gen.oneOf(Male, Female)
  } yield User(name, age, gndr)

  println(userGenerator.sample)
  println(userGenerator.sample)

  val userProperty = forAll(userGenerator) { user =>
    user.age > 0
  }
  userProperty.check


}
