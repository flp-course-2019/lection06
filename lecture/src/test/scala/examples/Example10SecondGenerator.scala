package examples

import org.scalacheck.Gen

object Example10SecondGenerator extends App {

  val vowel1: Gen[Char] = Gen.oneOf('A', 'E', 'I', 'O', 'U', 'Y')
  println(vowel1.sample)

  val vowel2: Gen[Char] = Gen.frequency(
    (3, 'A'),
    (4, 'E'),
    (2, 'I'),
    (3, 'O'),
    (1, 'U'),
    (1, 'Y')
  )
  println(vowel2.sample)



}
