package examples

import org.scalacheck.Prop.{forAll, BooleanOperators}

object Example04Implication {

  val propMakeList = forAll { n: Int =>
    (n >= 0 && n < 10000) ==> (List.fill(n)("").length == n)
  }

}
