package examples

import org.scalacheck.Prop._
import org.scalacheck.{Gen, Properties}

object Example17Classify extends Properties("classify") {

  def isOrdered(l: List[Int]): Boolean = l == l.sorted

  property("double reverse") = forAll { l: List[Int] =>
    classify(isOrdered(l), "ordered") {
      classify(l.length > 5, "large", "small") {
        l.reverse.reverse == l
      }
    }
  }

  property("dummy") = forAll(Gen.choose(1, 10)) { n =>
    collect(n) {
      n == n
    }
  }

}
