package examples

import org.scalacheck.Prop.{BooleanOperators, forAll}

object Example05Implication2 extends App {

  val propTrivial = forAll { n: Int =>
    (n == 0) ==> (n == 0)
  }
  propTrivial.check

  // ! Gave up after only 4 passed tests. 500 tests were discarded.

}
