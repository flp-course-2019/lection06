package examples

import org.scalacheck.Prop
import org.scalacheck.Prop.forAll

object Example02SecondProp extends App {

  val propSqrt: Prop = forAll { n: Int =>
    scala.math.sqrt(n * n) == n
  }

  propSqrt.check

  /*
   * ! Falsified after 2 passed tests.
   * > ARG_0: -1
   * > ARG_0_ORIGINAL: -488187735
   */

}
