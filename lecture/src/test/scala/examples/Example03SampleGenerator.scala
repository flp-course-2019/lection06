package examples

import org.scalacheck.{Gen, Prop}
import org.scalacheck.Prop.forAll

object Example03SampleGenerator {

  val smallInteger: Gen[Int] = Gen.choose(0,100)
  val propSmallInteger: Prop = forAll(smallInteger) { n =>
    n >= 0 && n <= 100
  }

}
