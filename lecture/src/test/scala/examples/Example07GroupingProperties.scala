package examples

import org.scalacheck.Properties
import org.scalacheck.Prop._

object StringSpecification extends Properties("String") {

  property("startsWith") = forAll { (a: String, b: String) =>
    (a+b).startsWith(a)
  }

  property("endsWith") = forAll { (a: String, b: String) =>
    (a+b).endsWith(b)
  }

  property("substring") = forAll { (a: String, b: String) =>
    (a+b).substring(a.length) == b
  }

  property("substring") = forAll { (a: String, b: String, c: String) =>
    (a+b+c).substring(a.length, a.length+b.length) == b
  }

}

object OtherPlace extends App {
  StringSpecification.check()
}

object MyAppSpecification extends Properties("MyApp") {
  include(StringSpecification)
  // include(???) etc
}