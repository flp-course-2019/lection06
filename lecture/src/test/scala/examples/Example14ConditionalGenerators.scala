package examples

import org.scalacheck.{Gen, Properties}

object Example14ConditionalGenerators extends Properties("conditional") {

  val smallEven: Gen[Int] = Gen.choose(0, 200).suchThat(_ % 2 == 0)

  /*
   * Gen.choose(0, 200)
   *   .withFilter(even => even % 2 == 0)
   *   .flatMap(even =>
   *     Gen.choose(0, 200)
   *       .withFilter(odd => odd % 2 != 0)
   *       .map(odd => (even, odd))
   *   )
   */

  val evenOdd: Gen[(Int, Int)] = for {
    even <- Gen.choose(0, 200) if even % 2 == 0
    odd  <- Gen.choose(0, 200) if odd  % 2 != 0
  } yield (even, odd)

  println(evenOdd.sample)
  println(smallEven.sample)




}
