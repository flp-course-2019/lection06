package examples

import org.scalacheck.Prop
import org.scalacheck.Prop.BooleanOperators

object Example01FirstProp {

  /*_*/
  val propSquare: Prop = Prop.forAll { x: Int =>
    x != 0 ==> x * x > 0
  }
  propSquare.check

  /*
   * + OK, passed 100 tests.
   */

}
