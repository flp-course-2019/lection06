package examples

import org.scalacheck.Prop
import org.scalacheck.Prop._

object Example06CombiningProperties {

  val p1: Prop = 1 == 1

  val p2: Prop = forAll { i: Int =>
    i > i-1
  }

  val p3: Prop = p1 && p2

  val p4: Prop = p1 || p2

  val p5: Prop = p1 == p2

  val p6: Prop = all(p1, p2)        // same as p1 && p2

  val p7: Prop = atLeastOne(p1, p2) // same as p1 || p2

}
