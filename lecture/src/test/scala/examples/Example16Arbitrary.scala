package examples

import org.scalacheck.{Arbitrary, Gen, Properties}
import org.scalacheck.Prop.forAll
import org.scalacheck.Gen.{choose, oneOf}

object Example16Arbitrary extends Properties("arbitrary") {

  val evenInteger: Gen[Int] = Arbitrary.arbitrary[Int].filter(_ % 2 == 0)

  val squares: Gen[List[Int]] = for {
    xs <- Arbitrary.arbitrary[List[Int]]
  } yield xs.map(x => x * x)

  implicit lazy val arbBool: Arbitrary[Boolean] = Arbitrary(oneOf(true, false))



  implicit lazy val arbTree: Arbitrary[Tree] = Arbitrary {
    val genLeaf = choose(0, 100).map(Leaf)

    def genNode(max: Int): Gen[Node] = for {
      left  <- genTree(max - 1)
      right <- genTree(max - 1)
    } yield Node(left, right)

    def genTree(max: Int): Gen[Tree] = if (max > 0) oneOf(genLeaf, genNode(max)) else genLeaf


    Gen.sized(genTree)
  }

  property("tree equals") = forAll { tree: Tree =>
    tree == tree
  }

}
