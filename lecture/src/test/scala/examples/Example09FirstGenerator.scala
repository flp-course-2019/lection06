package examples

import org.scalacheck.Gen

object Example09FirstGenerator extends App {

  /*
   * Gen.choose(10, 20).flatMap { n =>
   *   Gen.choose(2 * n, 500).map { m =>
   *     (n, m)
   *   }
   * }
   *
   */

  val myGen: Gen[(Int, Int)] = for {
    n <- Gen.choose(10, 20)
    m <- Gen.choose(2 * n, 500)
  } yield (n, m)

  println(myGen.sample)


}
