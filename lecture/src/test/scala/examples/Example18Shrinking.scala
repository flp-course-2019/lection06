package examples

import org.scalacheck.Prop._
import org.scalacheck.Arbitrary.arbitrary
import org.scalacheck.Properties

object Example18Shrinking extends Properties("shrinking") {

  property("p1") = forAllNoShrink(arbitrary[List[Int]])(l => l == l.distinct)

  property("p2") = forAll(arbitrary[List[Int]])(l => l == l.distinct)

  property("p3") = forAll( (l: List[Int]) => l == l.distinct )


}
