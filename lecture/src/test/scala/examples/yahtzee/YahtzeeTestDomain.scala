package examples.yahtzee

import org.scalacheck.Gen._
import org.scalacheck._
import Yahtzee._

import scala.util.Random

trait YahtzeeTestDomain {

  val allDice: List[Die] = List(One, Two, Three, Four, Five, Six)

  val genYahtzee: Gen[Hand] = oneOf(allDice).map(d => Hand(d, d, d, d, d))

  // выбираем один из возможных вариантов. 1-5 или 2-6 и генерируем случайную последовательность состоящую из этих кубиков.
  val genStraight: Gen[Hand] = {
    val hand1 = List(One, Two, Three, Four, Five)
    val hand2 = List(Two, Three, Four, Five, Six)

    for {
      dice <- oneOf(hand1, hand2)
      p1 <- oneOf(dice)
      next1 = dice diff List(p1)
      p2 <- oneOf(next1)
      next2 = next1 diff List(p2)
      p3 <- oneOf(next2)
      next3 = next2 diff List(p3)
      p4 <- oneOf(next3)
      next4 = next3 diff List(p4)
      p5 <- oneOf(next4)

      p1r :: p2r :: p3r :: p4r :: p5r :: _ = Random.shuffle(List(p1, p2, p3, p4, p5))
    } yield Hand(p1r, p2r, p3r, p4r, p5r)
  }

  val genFourOfAKind: Gen[Hand] = for {
    d1 <- oneOf(allDice)
    d2 <- oneOf(allDice diff List(d1))
  } yield Hand(d1, d1, d1, d1, d2)

  val genThreeOfAKind: Gen[Hand] = for {
    d1 <- oneOf(allDice)
    d2 <- oneOf(allDice diff List(d1))
    d3 <- oneOf(allDice diff List(d1, d2))
  } yield Hand(d1, d1, d1, d2, d3)

  val genFullHouse: Gen[Hand] = for {
    d1 <- oneOf(allDice)
    d2 <- oneOf(allDice diff List(d1))
  } yield Hand(d1, d1, d1, d2, d2)

  val orderedGenerators: List[Gen[Hand]] = List(
    genYahtzee,
    genStraight,
    genFullHouse,
    genFourOfAKind,
    genThreeOfAKind
  )

}
