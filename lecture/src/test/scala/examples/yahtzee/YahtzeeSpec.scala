package examples.yahtzee

import org.scalacheck.Gen._
import org.scalacheck.Prop._
import org.scalacheck._
import Yahtzee._

object YahtzeeSpec extends Properties("Yahtzee") with YahtzeeTestDomain {

  property("Winning hand is chosen correctly") = forAll(chooseNum[Int](1, orderedGenerators.length - 1)) { idx =>
    val (winningHandGenerator, losingHandGenerator) = {
      if(idx == 1) (genYahtzee, oneOf(genStraight, genFullHouse, genFourOfAKind, genThreeOfAKind))
      else if (idx == orderedGenerators.length - 1) (oneOf(genYahtzee, genStraight, genFullHouse, genFourOfAKind), genThreeOfAKind)
      else {
        val (wg1 :: wg2 :: wgn, lg1 :: lg2 :: lgn) = orderedGenerators.splitAt(idx)
        (oneOf(wg1, wg2, wgn: _*), oneOf(lg1, lg2, lgn: _*))
      }
    }

    forAll(winningHandGenerator, losingHandGenerator) { (wh, lh) =>
      collect(s"${wh.score} vs ${lh.score}") {
        (winner(wh, lh) ?= wh) &&
        (winner(lh, wh) ?= wh)
      }
    }
  }

}