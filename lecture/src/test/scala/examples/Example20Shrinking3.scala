package examples

import org.scalacheck.{Properties, _}
import org.scalacheck.Prop._
import org.scalacheck.Arbitrary.arbitrary

object Example20Shrinking3 extends Properties("shrink person") {

  case class Person(name: String, age: Int)
  implicit val arbPerson: Arbitrary[Person] = Arbitrary {
    for {
      name <- arbitrary[String]
      age  <- arbitrary[Int]
    } yield Person(name, age)
  }

//  implicit val shrhinkPerson: Shrink[Person] = Shrink { person =>
//    implicitly[Shrink[(String, Int)]].shrink((person.name, person.age)) // мы можем позвать готовый шринкер для тупла строк и интов
//      .map((Person.apply _).tupled)                                     // стрим полученных значений мы можем отобразить в Person.
//  }

  property("up then down is the same as down") = forAll { person: Person =>
    person.name.toUpperCase.toLowerCase == person.name.toLowerCase
  }

}
