package examples

import org.scalacheck.{Gen, Properties}
import org.scalacheck.Gen._
import org.scalacheck.Prop._

object Example13SizedGenerators extends Properties("matrix") {

  def matrix[T](g: Gen[T]): Gen[Seq[Seq[T]]] = Gen.sized { size =>
    val side = scala.math.sqrt(size).asInstanceOf[Int]
    Gen.listOfN(side, Gen.listOfN(side, g))
  }

  property("matrix example") = forAll(matrix(choose(0, 1))) { m =>
    val pretty = m.map(_.mkString(" ")).mkString("\n")
    collect("\n" + pretty + "\n")(m == m)
  }




}
